# IFTTT Bakalari Utils

#### Modules
- info_o_nove_hodine.py 
    - Triggers IFTTT webhook 10 minutes before the lesson starts. 
                          value1=jmeno_hodiny, value2=ucebna
    - Can be used for example for sending notification where the next lesson is. 
    - This module must be running in background to work.
                          